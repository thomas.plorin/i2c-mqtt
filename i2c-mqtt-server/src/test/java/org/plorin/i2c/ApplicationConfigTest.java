package org.plorin.i2c;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.plorin.i2c.device.dev.GpioControllerDev;
import org.plorin.i2c.device.dev.I2CBusDev;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.i2c.I2CBus;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationConfigTest {

	@Mock
	private Environment environment;

	@Spy
	private AnnotationConfigApplicationContext applicationContext;

	@InjectMocks
	private ApplicationConfig applicationConfig;

	@Before
	public void setup() {

	}

	@Test
	public void testCreateI2CBus() throws Exception {
		Mockito.when(environment.getActiveProfiles()).thenReturn(new String[] { "dev" });
		I2CBus createI2CBus = applicationConfig.createI2CBus();
		Assert.assertThat(createI2CBus, Matchers.is(Matchers.instanceOf(I2CBusDev.class)));
	}

	@Test
	public void testCreateGpioController() throws Exception {
		Mockito.when(environment.getActiveProfiles()).thenReturn(new String[] { "dev" });
		GpioController createGpioController = applicationConfig.createGpioController();
		Assert.assertThat(createGpioController, Matchers.is(Matchers.instanceOf(GpioControllerDev.class)));
	}

	@Test
	public void testClose() throws Exception {
		applicationConfig.close();
		Mockito.verify(applicationContext, Mockito.times(1)).close();
	}

	@Test
	public void testIsDevMode() throws Exception {
		Mockito.when(environment.getActiveProfiles()).thenReturn(new String[] {});
		Assert.assertFalse(applicationConfig.isDevMode());
	}

	@Test
	public void testIsNotDevMode() throws Exception {
		Mockito.when(environment.getActiveProfiles()).thenReturn(new String[] { "dev" });
		Assert.assertTrue(applicationConfig.isDevMode());
	}

}
