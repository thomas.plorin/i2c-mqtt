package org.plorin.i2c.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class BetterByteTest {

	private BetterByte betterByte;

	@Test
	public void isSetTest() {
		// All on
		betterByte = BetterByte.of(0xff);
		Arrays.stream(new int[] { 1, 2, 3, 4, 5, 6, 7, 8 }).forEach(v -> assertTrue(betterByte.isSet(v)));

		// All off
		betterByte = BetterByte.of(0x00);
		Arrays.stream(new int[] { 1, 2, 3, 4, 5, 6, 7, 8 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

		// Bit 1
		betterByte = BetterByte.of(1);
		Arrays.stream(new int[] { 1 }).forEach(v2 -> assertTrue(betterByte.isSet(v2)));
		Arrays.stream(new int[] { 2, 3, 4, 5, 6, 7, 8 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

		// Bit 2
		betterByte = BetterByte.of(2);
		Arrays.stream(new int[] { 2 }).forEach(v2 -> assertTrue(betterByte.isSet(v2)));
		Arrays.stream(new int[] { 1, 3, 4, 5, 6, 7, 8 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

		// Bit 3
		betterByte = BetterByte.of(4);
		Arrays.stream(new int[] { 3 }).forEach(v2 -> assertTrue(betterByte.isSet(v2)));
		Arrays.stream(new int[] { 1, 2, 4, 5, 6, 7, 8 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

		// Bit 4
		betterByte = BetterByte.of(8);
		Arrays.stream(new int[] { 4 }).forEach(v2 -> assertTrue(betterByte.isSet(v2)));
		Arrays.stream(new int[] { 1, 2, 3, 5, 6, 7, 8 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

		// Bit 5
		betterByte = BetterByte.of(16);
		Arrays.stream(new int[] { 5 }).forEach(v2 -> assertTrue(betterByte.isSet(v2)));
		Arrays.stream(new int[] { 1, 2, 3, 4, 6, 7, 8 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

		// Bit 6
		betterByte = BetterByte.of(32);
		Arrays.stream(new int[] { 6 }).forEach(v2 -> assertTrue(betterByte.isSet(v2)));
		Arrays.stream(new int[] { 1, 2, 3, 4, 5, 7, 8 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

		// Bit 7
		betterByte = BetterByte.of(64);
		Arrays.stream(new int[] { 7 }).forEach(v2 -> assertTrue(betterByte.isSet(v2)));
		Arrays.stream(new int[] { 1, 2, 3, 4, 5, 6, 8 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

		// Bit 8
		betterByte = BetterByte.of(128);
		Arrays.stream(new int[] { 8 }).forEach(v2 -> assertTrue(betterByte.isSet(v2)));
		Arrays.stream(new int[] { 1, 2, 3, 4, 5, 6, 7 }).forEach(v2 -> assertFalse(betterByte.isSet(v2)));

	}

	@Test
	public void ofStringTest() {
		byte toByte = BetterByte.of("0x38").get();
		Assert.assertEquals(0x38, toByte);
	}

	@Test
	public void setBitTest() {

		Arrays.stream(new int[] { 1, 2, 3, 4, 5, 6, 7, 8 }).forEach(position -> {
			BetterByte betterByte = BetterByte.of(0);
			betterByte = betterByte.setBit(position, true);
			System.out.println((int) betterByte.getInteger());
			assertTrue("Fail at " + position, betterByte.getInteger() == (int) Math.pow(2, position - 1));
		});

	}

	@Test
	public void unsetBitTest() {

		Arrays.stream(new int[] { 8, 7, 6, 5, 4, 3, 2, 1 }).forEach(position -> {
			BetterByte betterByte = BetterByte.of(255);
			betterByte = betterByte.setBit(position, false);
//			System.out.println((int) betterByte.getInteger());
			int expectedValue = 255 - (int) Math.pow(2, position - 1);
			assertTrue(betterByte.getInteger() == expectedValue);
		});

	}

	@Test
	public void invertTest() {
		BetterByte of = BetterByte.of(255);
		Assert.assertTrue(of.invert().getInteger() == 0);
		
		BetterByte of1 = BetterByte.of(0);
		Assert.assertTrue(of1.invert().getInteger() == 255);
		
		BetterByte of2 = BetterByte.of(15);
		Assert.assertTrue(of2.invert().getInteger() == 240);

		
	}

}
