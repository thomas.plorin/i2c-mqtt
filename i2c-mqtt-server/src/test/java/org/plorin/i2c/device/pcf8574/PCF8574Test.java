package org.plorin.i2c.device.pcf8574;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.plorin.i2c.device.I2CDeviceType;
import org.plorin.i2c.device.pcf8574.PCF8574.InputOutput;
import org.plorin.i2c.mqtt.MqttConnector;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;

public class PCF8574Test {

	private PCF8574 pcf8574 = new PCF8574();

	private I2CBus i2cBus;

	private I2CDevice i2cDevice;

	private MqttConnector mqttConnector;

	@Before
	public void setup() throws IOException {
		i2cDevice = Mockito.mock(I2CDevice.class);
		i2cBus = Mockito.mock(I2CBus.class);
		mqttConnector = Mockito.mock(MqttConnector.class);
		Mockito.when(i2cBus.getDevice(Mockito.anyInt())).thenReturn(i2cDevice);
	}

	@Test
	public void testInput1() throws Exception {

		pcf8574.init(i2cBus, mqttConnector, "pcf8574:testinstance:0x38:input:false");
		Mockito.when(i2cDevice.read()).thenReturn(Integer.valueOf(85));

		// Wert einlesen auslösen
		pcf8574.interrupt();

		ArgumentCaptor<String> mqttCaptor1 = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> mqttCaptor2 = ArgumentCaptor.forClass(String.class);
		Mockito.verify(mqttConnector, Mockito.times(4)).send(mqttCaptor1.capture(), mqttCaptor2.capture());

//		System.out.println(Arrays.toString(mqttCaptor1.getAllValues().toArray()));
//		System.out.println(Arrays.toString(mqttCaptor2.getAllValues().toArray()));

		assertEquals(asList("i2c/input/testinstance/1/STAT", "i2c/input/testinstance/3/STAT",
				"i2c/input/testinstance/5/STAT", "i2c/input/testinstance/7/STAT"), mqttCaptor1.getAllValues());

		assertEquals(asList("ON", "ON", "ON", "ON"), mqttCaptor2.getAllValues());

	}

	@Test
	public void testInput2() throws Exception {

		pcf8574.init(i2cBus, mqttConnector, "pcf8574:testinstance:0x38:input:false");

		Mockito.when(i2cDevice.read()).thenReturn(Integer.valueOf(255));

		// Wert einlesen auslösen
		pcf8574.interrupt();

		Mockito.when(i2cDevice.read()).thenReturn(Integer.valueOf(85));

		pcf8574.interrupt();

		ArgumentCaptor<String> mqttCaptor1 = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> mqttCaptor2 = ArgumentCaptor.forClass(String.class);
		Mockito.verify(mqttConnector, Mockito.times(12)).send(mqttCaptor1.capture(), mqttCaptor2.capture());

//		System.out.println(Arrays.toString(mqttCaptor1.getAllValues().toArray()));
//		System.out.println(Arrays.toString(mqttCaptor2.getAllValues().toArray()));

		assertEquals(asList("i2c/input/testinstance/1/STAT", "i2c/input/testinstance/2/STAT",
				"i2c/input/testinstance/3/STAT", "i2c/input/testinstance/4/STAT", "i2c/input/testinstance/5/STAT",
				"i2c/input/testinstance/6/STAT", "i2c/input/testinstance/7/STAT", "i2c/input/testinstance/8/STAT",
				"i2c/input/testinstance/2/STAT", "i2c/input/testinstance/4/STAT", "i2c/input/testinstance/6/STAT",
				"i2c/input/testinstance/8/STAT"), mqttCaptor1.getAllValues());

		assertEquals(asList("ON", "ON", "ON", "ON", "ON", "ON", "ON", "ON", "OFF", "OFF", "OFF", "OFF"),
				mqttCaptor2.getAllValues());

	}

	@Test
	public void testOutput() throws Exception {

		pcf8574.init(i2cBus, mqttConnector, "pcf8574:testinstance:0x38:output:false");

		ArgumentCaptor<String> captorTopic = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<IMqttMessageListener> captorListener = ArgumentCaptor.forClass(IMqttMessageListener.class);
		Mockito.verify(mqttConnector, Mockito.times(1)).subscribe(captorTopic.capture(), captorListener.capture());

		IMqttMessageListener listener = captorListener.getValue();

		String topic = String.format(PCF8574.TOPIC_POWER, "output", "testoutput", "1");
		MqttMessage mqttMessage = new MqttMessage("on".getBytes());
		listener.messageArrived(topic, mqttMessage);

		topic = String.format(PCF8574.TOPIC_POWER, "output", "testoutput", "1");
		mqttMessage = new MqttMessage("off".getBytes());
		listener.messageArrived(topic, mqttMessage);

		ArgumentCaptor<String> mqttCaptor1 = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> mqttCaptor2 = ArgumentCaptor.forClass(String.class);
		Mockito.verify(mqttConnector, Mockito.times(2)).send(mqttCaptor1.capture(), mqttCaptor2.capture());

		System.out.println(Arrays.toString(mqttCaptor1.getAllValues().toArray()));
		System.out.println(Arrays.toString(mqttCaptor2.getAllValues().toArray()));

//		assertEquals(asList("/i2c/input/testinstance/1/STAT", "/i2c/input/testinstance/2/STAT",
//				"/i2c/input/testinstance/3/STAT", "/i2c/input/testinstance/4/STAT", "/i2c/input/testinstance/5/STAT",
//				"/i2c/input/testinstance/6/STAT", "/i2c/input/testinstance/7/STAT", "/i2c/input/testinstance/8/STAT",
//				"/i2c/input/testinstance/2/STAT", "/i2c/input/testinstance/4/STAT", "/i2c/input/testinstance/6/STAT",
//				"/i2c/input/testinstance/8/STAT"), mqttCaptor1.getAllValues());
//
//		assertEquals(asList("ON", "ON", "ON", "ON", "ON", "ON", "ON", "ON", "OFF", "OFF", "OFF", "OFF"),
//				mqttCaptor2.getAllValues());

//		Mockito.when(i2cdevice.read()).thenReturn(Integer.valueOf(255));

	}

	@Test
	public void testShutdown() throws Exception {

	}

	@Test
	public void testConfig() throws Exception {
		pcf8574.init(i2cBus, mqttConnector, "pcf8574:testinstance:0x38:output:false");
		Assert.assertThat(pcf8574.getInstanceName(), Matchers.is("testinstance"));
		Assert.assertTrue(pcf8574.getAddress() == 0x38);
		Assert.assertThat(pcf8574.getInOrOut(), Matchers.is(InputOutput.OUTPUT));
		Assert.assertThat(pcf8574.isInverted(), Matchers.is(Boolean.FALSE));
	}

	@Test
	public void testGetType() throws Exception {
		PCF8574 pcf8574 = new PCF8574();
		Assert.assertThat(pcf8574.getType(), Matchers.is(I2CDeviceType.PCF8574));
	}

}
