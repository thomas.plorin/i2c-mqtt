package org.plorin.i2c.device;

import org.junit.Assert;
import org.junit.Test;

public class I2CBusNameTest {

	@Test
	public void testLookup() throws Exception {
		for (int i = 0; i < 18; i++) {
			String busname = "bus_" + i;
			I2CBusName lookup = I2CBusName.lookup(busname);
			Assert.assertTrue(lookup.name().equalsIgnoreCase(busname));
		}
	}

	@Test
	public void testGetI2cBusNr() throws Exception {
		for (int i = 0; i < 18; i++) {
			String busname = "bus_" + i;
			I2CBusName lookup = I2CBusName.lookup(busname);
			Assert.assertTrue(lookup.getI2cBusNr() == i);
		}
	}

}
