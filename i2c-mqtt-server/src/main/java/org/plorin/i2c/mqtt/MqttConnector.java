package org.plorin.i2c.mqtt;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.plorin.i2c.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class MqttConnector {

	private ApplicationProperties applicationProperties;

	private IMqttClient publisher;

	private Map<String, IMqttMessageListener> subscribeTopics = new HashMap<>();

	MqttCallback mqttCallback = new MqttCallback() {
		public void connectionLost(Throwable t) {
			log.warn("Mqtt Connection lost, try to reconnect", t);
			while (!publisher.isConnected()) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					log.error(e);
				}
				MqttConnector.this.connect();
				MqttConnector.this.resubscribe();
			}
			log.info("reconnected to mqtt server");
		}

		public void messageArrived(String topic, MqttMessage message) throws Exception {
			log.debug("topic - " + topic + ": " + new String(message.getPayload()));
		}

		public void deliveryComplete(IMqttDeliveryToken token) {
		}
	};

	@Autowired
	public MqttConnector(ApplicationProperties c) {
		this.applicationProperties = c;
		connect();
	}

	private void resubscribe() {
		for (Map.Entry<String, IMqttMessageListener> entry : subscribeTopics.entrySet()) {
			log.info("Resubscribe to {}", entry.getKey());
			this.subscribe(entry.getKey(), entry.getValue());
		}

	}

	public void connect() {
		String publisherId = UUID.randomUUID().toString();
		try {
			String mqttHost = applicationProperties.getMqttServer();
			log.debug("MQTT-SERVER:" + mqttHost);
			publisher = new MqttClient("tcp://" + mqttHost, publisherId);
			publisher.setCallback(mqttCallback);
			MqttConnectOptions options = new MqttConnectOptions();
			options.setAutomaticReconnect(applicationProperties.isAutomaticReconnect());
			options.setCleanSession(applicationProperties.isCleanSession());
			options.setConnectionTimeout(applicationProperties.getConnectionTimeout());
			if (applicationProperties.isCredentialsSet()) {
				options.setUserName(applicationProperties.getUsername());
				options.setPassword(applicationProperties.getPassword().toCharArray());
			}
			log.info("Connect to {}", mqttHost);
			publisher.connect(options);
		} catch (MqttException e) {
			log.error(e);
		}

	}

	public void shutdown() {
		try {
			publisher.close();
		} catch (MqttException e) {
			log.error(e);
		}
	}

	public void send(String topic, String msg) {
		MqttMessage msg2 = new MqttMessage(msg.getBytes());
		msg2.setQos(applicationProperties.getMqttQos());
		msg2.setRetained(applicationProperties.isMqttRetained());
		try {
			log.debug("Publish start");
			MqttTopic topic2 = publisher.getTopic(topic);
			topic2.publish(msg2);
			log.debug("Publish end");
		} catch (MqttException e) {
			log.error(e);
		}
	}

	public void subscribe(String topic, IMqttMessageListener listener) {
		try {
			log.debug("MQTT: Subscribe to topic: " + topic);
			subscribeTopics.put(topic, listener);
			publisher.subscribe(topic, listener);
		} catch (MqttException e) {
			log.error(e);
		}
	}

}
