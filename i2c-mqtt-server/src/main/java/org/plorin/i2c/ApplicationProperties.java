package org.plorin.i2c;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;

@Configuration
@PropertySource(value = { "classpath:app.properties", "file:./app.properties" }, ignoreResourceNotFound = true)
@Getter
public class ApplicationProperties {

	@Value("#{'${i2c.devices}'.split(',')}")
	private List<String> i2cDeviceConfigs = new ArrayList<String>();

	@Value("${i2c.busname}")
	private String i2cBusName;

	@Value("${mqtt.server}")
	private String mqttServer;

	@Value("${mqtt.username:}")
	private String username;

	@Value("${mqtt.password:}")
	private String password;

	@Value("${mqtt.retained}")
	private boolean mqttRetained;

	@Value("${mqtt.qos}")
	private int mqttQos;

	@Value("${mqtt.automaticReconnect}")
	private boolean automaticReconnect;

	@Value("${mqtt.cleanSession}")
	private boolean cleanSession;

	@Value("${mqtt.connectionTimeout}")
	private int connectionTimeout;

	public boolean isCredentialsSet() {
		boolean usernameSet = !StringUtils.isEmpty(username);
		boolean passwordSet = !StringUtils.isEmpty(password);
		return usernameSet && passwordSet;
	}

}
