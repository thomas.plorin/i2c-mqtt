package org.plorin.i2c.device;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.Validate;

import com.pi4j.io.i2c.I2CBus;

public enum I2CBusName {

	BUS_0("bus_0", I2CBus.BUS_0), BUS_1("bus_1", I2CBus.BUS_1), BUS_2("bus_2", I2CBus.BUS_2),
	BUS_3("bus_3", I2CBus.BUS_3), BUS_4("bus_4", I2CBus.BUS_4), BUS_5("bus_5", I2CBus.BUS_5),
	BUS_6("bus_6", I2CBus.BUS_6), BUS_7("bus_7", I2CBus.BUS_7), BUS_8("bus_8", I2CBus.BUS_8),
	BUS_9("bus_9", I2CBus.BUS_9), BUS_10("bus_10", I2CBus.BUS_10), BUS_11("bus_11", I2CBus.BUS_11),
	BUS_12("bus_12", I2CBus.BUS_12), BUS_13("bus_13", I2CBus.BUS_13), BUS_14("bus_14", I2CBus.BUS_14),
	BUS_15("bus_15", I2CBus.BUS_15), BUS_16("bus_16", I2CBus.BUS_16), BUS_17("bus_17", I2CBus.BUS_17);

	private String busname;

	private int busNr;

	private static Map<String, I2CBusName> LOOKUP = new HashMap<String, I2CBusName>();

	static {
		for (I2CBusName i2cBusName : I2CBusName.values())
			LOOKUP.put(i2cBusName.busname.toLowerCase(), i2cBusName);
	}

	private I2CBusName(String busname, int busNr) {
		Validate.notNull(busname);
		Validate.isTrue(busNr >= 0 && busNr < 18);
		this.busname = busname;
		this.busNr = busNr;
	}

	public static I2CBusName lookup(final String busname) {
		I2CBusName i2cBusName = LOOKUP.get(busname.toLowerCase());
		Validate.notNull(i2cBusName);
		return i2cBusName;
	}

	public int getI2cBusNr() {
		return busNr;
	}
}
