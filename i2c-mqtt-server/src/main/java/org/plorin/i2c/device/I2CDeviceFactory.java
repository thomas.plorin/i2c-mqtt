package org.plorin.i2c.device;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.plorin.i2c.ApplicationProperties;
import org.plorin.i2c.device.pcf8574.PCF8574;
import org.plorin.i2c.mqtt.MqttConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.i2c.I2CBus;

import lombok.extern.log4j.Log4j2;

@Component

@Log4j2
public class I2CDeviceFactory {

	@Autowired
	private GpioController gpioController;

	@Autowired
	private ApplicationProperties config;

	@Autowired
	private MqttConnector mqtt;

	@Autowired
	private I2CBus i2cbus;

	private Map<String, I2CMQTTDevice> deviceMap = new HashMap<>();

	public void shutdown() {
		try {
			deviceMap.values().stream().forEach(I2CMQTTDevice::shutdown);
			i2cbus.close();
			gpioController.shutdown();
		} catch (IOException e) {
			log.error(e);
		}
	}

	public void init() {
		initDevices();
		prepareInterruptListener();
	}

	private void initDevices() {
		config.getI2cDeviceConfigs().stream().forEach(deviceConfigString -> {
			String[] attributes = deviceConfigString.split(":");
			String type = attributes[0];
			I2CDeviceType lookup = I2CDeviceType.lookup(type);
			Validate.notNull(lookup);
			switch (lookup) {
			case PCF8574: {
				PCF8574 p = new PCF8574();
				p.init(i2cbus, mqtt, deviceConfigString);
				deviceMap.put(p.getInstanceName(), p);
				break;
			}
			}
		});
	}

	private void prepareInterruptListener() {

		final GpioPinDigitalInput myButton = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_00,
				PinPullResistance.PULL_DOWN);
		myButton.setShutdownOptions(true);
		myButton.addListener(new GpioPinListenerDigital() {
			@Override
			public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
				log.debug(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
				if (event.getState().isHigh()) {
					deviceMap.values().stream().forEach(I2CMQTTDevice::interrupt);
				}
			}
		});
	}
}
