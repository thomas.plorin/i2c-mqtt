package org.plorin.i2c.device;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.Validate;

public enum I2CDeviceType {
	
	PCF8574("pcf8574");

	private String typeName;

	private static Map<String, I2CDeviceType> LOOKUP = new HashMap<String, I2CDeviceType>();

	static {
		for (I2CDeviceType type : I2CDeviceType.values()) {
			LOOKUP.put(type.getTypName(), type);
		}
	}

	private I2CDeviceType(String typeName) {
		Validate.notNull(typeName);
		this.typeName = typeName.toLowerCase();
	}

	public String getTypName() {
		return typeName;
	}

	public static I2CDeviceType lookup(final String typeName) {
		return LOOKUP.get(typeName.toLowerCase());
	}
}
