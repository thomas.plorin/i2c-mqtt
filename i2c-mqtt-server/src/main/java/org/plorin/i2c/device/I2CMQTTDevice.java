package org.plorin.i2c.device;

import org.plorin.i2c.mqtt.MqttConnector;

import com.pi4j.io.i2c.I2CBus;

public interface I2CMQTTDevice {

	void init(I2CBus i2cbus, MqttConnector mqtt, String deviceConfigString);

	String getInstanceName();

	void interrupt();

	void shutdown();

	I2CDeviceType getType();

}