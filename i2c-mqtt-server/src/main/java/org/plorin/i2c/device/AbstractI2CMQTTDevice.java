package org.plorin.i2c.device;

import java.io.IOException;

import org.apache.commons.lang3.Validate;
import org.plorin.i2c.mqtt.MqttConnector;
import org.plorin.i2c.utils.BetterByte;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;

import lombok.extern.log4j.Log4j2;

@Log4j2
public abstract class AbstractI2CMQTTDevice implements I2CMQTTDevice {

	protected MqttConnector mqtt;

	protected I2CBus i2cbus;

	protected String instanceName;

	protected byte address;

	protected I2CDevice device;

	@Override
	public void init(I2CBus i2cbus, MqttConnector mqtt, String deviceConfigString) {
		Validate.notNull(i2cbus);
		Validate.notNull(mqtt);
		Validate.notNull(deviceConfigString);
		this.i2cbus = i2cbus;
		this.mqtt = mqtt;

		String[] split = deviceConfigString.split(":");
		Validate.isTrue(split.length >= 3);
		this.instanceName = split[1].toLowerCase();
		this.address = BetterByte.of(split[2]).get();

		openI2CConnection();

	}

	private void openI2CConnection() {
		try {
			device = i2cbus.getDevice(this.address);
		} catch (IOException e) {
			log.error(e);
		}
	}

	public String getInstanceName() {
		return instanceName;
	}

	public byte getAddress() {
		return address;
	}

}
