package org.plorin.i2c.device.pcf8574;

import java.io.IOException;

import org.apache.commons.lang3.Validate;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.plorin.i2c.device.AbstractI2CMQTTDevice;
import org.plorin.i2c.device.I2CDeviceType;
import org.plorin.i2c.mqtt.MqttConnector;
import org.plorin.i2c.utils.BetterByte;

import com.pi4j.io.i2c.I2CBus;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class PCF8574 extends AbstractI2CMQTTDevice {

	enum InputOutput {
		INPUT, OUTPUT
	}

	static String TOPIC_POWER = "i2c/%s/%s/%s/POWER";

	static String TOPIC_STAT = "i2c/%s/%s/%s/STAT";

	private InputOutput inOrOut;

	private boolean inverted;

	private BetterByte lastValue = BetterByte.of(0);

	@Override
	public I2CDeviceType getType() {
		return I2CDeviceType.PCF8574;
	}

	public InputOutput getInOrOut() {
		return inOrOut;
	}

	public boolean isInverted() {
		return inverted;
	}

	@Override
	public void init(I2CBus i2cbus, MqttConnector mqtt, String deviceConfigString) {
		super.init(i2cbus, mqtt, deviceConfigString);

		// Special Config Parsing
		Validate.notNull(deviceConfigString);
		String[] split = deviceConfigString.split(":");
		Validate.isTrue(split.length == 5);
		Validate.isTrue(split[0].contentEquals(I2CDeviceType.PCF8574.getTypName()));

		if (split[3].equalsIgnoreCase("input"))
			this.inOrOut = InputOutput.INPUT;
		else if (split[3].equalsIgnoreCase("output"))
			this.inOrOut = InputOutput.OUTPUT;
		this.inverted = Boolean.valueOf(split[4]);

		log.info("init device [{}] with address [{}] as [{}]; inverted=[{}]", this.instanceName,
				"0x" + Integer.toHexString(this.address), this.inOrOut, this.inverted);

		if (InputOutput.OUTPUT.equals(this.inOrOut)) {
			addOutputListener();
		} else if (InputOutput.INPUT.equals(this.inOrOut)) {
			log.info("Input changes will be published to [{}]", createTopic(TOPIC_STAT, "[1-8]"));
		}
	}

	@Override
	public void shutdown() {
		log.info(String.format("Device %s shutdown", this.getInstanceName()));
	}

	@Override
	public void interrupt() {
		if (InputOutput.INPUT.equals(this.inOrOut)) {
			log.debug("INTERRUPT ON " + instanceName);
			final BetterByte inputByte = read();
			if (inputByte != null) {
				log.debug("READ BYTE:" + inputByte.getInteger());
				notifiyPinChange(inputByte);
				lastValue = inputByte;
			}
		}
	}

	private void addOutputListener() {
		if (InputOutput.OUTPUT.equals(this.inOrOut)) {
			String topic = createTopic(TOPIC_POWER, "+");
			log.info("Device [{}] subscribes to topic [{}]", this.instanceName, topic);
			mqtt.subscribe(topic, new IMqttMessageListener() {

				@Override
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					log.debug("Message " + topic);
					String[] split2 = topic.split("\\/");
					String ioPortNumber = split2[split2.length - 2];
					log.debug("IO Port Number:" + ioPortNumber);
					int portNumber = Integer.parseInt(ioPortNumber);
					log.debug("IO Port Number:" + portNumber);
					String payload = new String(message.getPayload());
					log.debug("Payload:" + payload);
					boolean onOff = "on".equalsIgnoreCase(payload);
					BetterByte newValue = lastValue.setBit(portNumber, onOff);
					writeOutput(newValue, false);
					log.debug("Message End");
				}
			});
			writeOutput(lastValue, true);
		}
	}

	private void notifiyPinChange(final BetterByte newValue) {
		for (int pos = 1; pos <= 8; pos++) {
			boolean lastValueSet = lastValue.isSet(pos);
			boolean actValueSet = newValue.isSet(pos);
			if (lastValueSet != actValueSet) {
				String createTopic = createTopic(TOPIC_STAT, pos);
				String msg = "ON";
				if (actValueSet)
					msg = "ON";
				else
					msg = "OFF";
				log.debug("Send Message to " + createTopic + " with Payload " + msg);
				mqtt.send(createTopic, msg);
				log.debug("Back from mqtt client");
			}
		}

	}

	private BetterByte read() {

		BetterByte inputByte = null;
		try {
			int read = device.read();
			if (inverted)
				read = 255 - read;
			inputByte = BetterByte.of(read);

		} catch (IOException e) {
			log.error("Fehler beim Auslesen des i2c Busses", e);
		}
		return inputByte;
	}

	private void writeOutput(BetterByte b, boolean silent) {
		try {
			log.debug("Write to i2c output:" + b.get());
			if (inverted) {
				log.debug("Write inverted: " + b.invert().get());
				device.write(b.invert().get());
			} else {
				log.debug("Write " + b.get());
				device.write(b.get());
			}
			if (!silent)
				notifiyPinChange(b);
			lastValue = b;
		} catch (IOException e) {
			log.error(e);
		}
	}

	private String createTopic(String topicTemplate, int portNumber) {
		String topic = createTopic(topicTemplate, Integer.toString(portNumber));
		return topic;
	}

	private String createTopic(String topicTemplate, String portNumber) {
		String topic = String.format(topicTemplate, this.inOrOut.name().toLowerCase(), this.instanceName, portNumber);
		return topic;
	}

}
