package org.plorin.i2c.device.dev;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;

public class I2CBusDev implements I2CBus {

	private Map<Integer, I2CDevice> deviceMap = new HashMap<>();

	private int bus;

	public I2CBusDev(int bus) {
		this.bus = bus;
	}

	@Override
	public I2CDevice getDevice(int address) throws IOException {
		Integer addressObj = Integer.valueOf(address);
		I2CDevice i2cDevice = deviceMap.get(addressObj);
		if (i2cDevice == null) {
			i2cDevice = new I2CDeviceDev();
			deviceMap.put(addressObj, i2cDevice);
		}
		return i2cDevice;
	}

	@Override
	public int getBusNumber() {
		return bus;
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub

	}

}
