package org.plorin.i2c;

import org.plorin.i2c.device.I2CDeviceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class I2C2MQTTServer {

	@Autowired
	private I2CDeviceFactory deviceFactory;

	@Autowired
	private ApplicationConfig appconfig;

	public static void main(String[] args) {
		log.info("START I2C-MQTT-SERVER");
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
		I2C2MQTTServer application = context.getBean(I2C2MQTTServer.class);
		application.run(context);
	}

	private void run(AnnotationConfigApplicationContext context) {

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				log.info("SHUTDOWN I2C-MQTT-SERVER");
				I2C2MQTTServer.this.shutdown();
			}
		});

		deviceFactory.init();

	}

	private void shutdown() {
		deviceFactory.shutdown();
		appconfig.close();
	}
}
