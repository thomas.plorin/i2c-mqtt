package org.plorin.i2c;

import java.io.IOException;
import java.util.Arrays;

import org.plorin.i2c.device.I2CBusName;
import org.plorin.i2c.device.dev.GpioControllerDev;
import org.plorin.i2c.device.dev.I2CBusDev;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

@Configuration
@ComponentScan(basePackages = "org.plorin")
public class ApplicationConfig {

	@Autowired
	private Environment environment;

	@Autowired
	private AnnotationConfigApplicationContext applicationContext;

	@Autowired
	private ApplicationProperties properties;

	private Boolean devMode = null;

	@Bean
	public I2CBus createI2CBus() throws UnsupportedBusNumberException, IOException {
		if (isDevMode())
			return new I2CBusDev(I2CBus.BUS_1);
		else {
			I2CBusName i2cBusName = I2CBusName.lookup(properties.getI2cBusName());
			return I2CFactory.getInstance(i2cBusName.getI2cBusNr());
		}
	}

	@Bean
	public GpioController createGpioController() {
		if (isDevMode()) {
			return new GpioControllerDev();
		} else {
			GpioController gpioController = GpioFactory.getInstance();
			return gpioController;

		}
	}

	public void close() {
		applicationContext.close();
	}

	boolean isDevMode() {
		if (devMode == null) {
			if (Arrays.stream(environment.getActiveProfiles()).anyMatch(env -> (env.equalsIgnoreCase("dev")))) {
				devMode = Boolean.TRUE;
			}
			if (devMode == null) {
				devMode = Boolean.FALSE;
			}
		}
		return devMode.booleanValue();
	}

}