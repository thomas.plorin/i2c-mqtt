package org.plorin.i2c.utils;

import org.apache.commons.lang3.Validate;

public class BetterByte {

	public static BetterByte of(int b) {
		return new BetterByte(b);
	}

	private int b;

	private BetterByte(int b) {
		this.b = b;
	}

	public byte get() {
		return (byte) b;
	}

	public int getInteger() {
		return b;
	}

	public boolean isSet(int position) {
		validatePositionIndex(position);
		int intMask = (int) Math.pow(2, position-1);
		byte mask = (byte) intMask;
		boolean res = (b & mask) == intMask;
		return res;
	}

	private void validatePositionIndex(int position) {
		Validate.isTrue(position >= 1 && position <= 8);
	}

	public static BetterByte of(String hex) {
		if (hex.toLowerCase().startsWith("0x")) {
			hex = hex.substring(2);
		}
		if (hex.length() != 2)
			throw new IllegalArgumentException("Hex String to valid. length <> 2");
		int parseInt = Integer.parseInt(hex, 16);
		return BetterByte.of(parseInt);
	}

	public BetterByte setBit(int position, boolean onOff) {
		int res = b;
		validatePositionIndex(position);
		int i = 1 << (position - 1);
		if (onOff) {
			res = b | i;
		} else {
			res = b & ~i;
		}
		return BetterByte.of(res);
	}
	
	public BetterByte invert() {
		int nw = 255 - this.b;
		return BetterByte.of(nw);
	}

}
