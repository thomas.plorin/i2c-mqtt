# i2c mqtt bridge server 1.1.2

"Bridges" the i2c Bus to mqtt topics.

# Prerequisites

- Raspberry PI with installed Raspbian
- PCF8574 Devices Connected to the I2C BUS from Raspberry PI
- Installed Java-JRE (recommended : oracle java 8)

## Enable i2c-bus in rasp-config

1. Start raspi-config
2. -> Interface - Options
3. -> I2C
4. -> Enable "YES"

## Install wiringpi

```
sudo apt-get install wiringpi
```

## Install oracle-jdk8
(Not tested with other jre's)

```
sudo apt-get install oracle-java8-jdk
```

## Download or Build From Source

Download latest (i2c-mqtt-server-linux-1.1.2.jar) Linux-Version from [Package Registry](https://gitlab.com/thomas.plorin/i2c-mqtt/-/packages)

## Create (copy app.properties) to jar-File-Folder

[app.properties](https://gitlab.com/thomas.plorin/i2c-mqtt/raw/master/i2c-mqtt-server/src/main/resources/app.properties)

```
wget https://gitlab.com/thomas.plorin/i2c-mqtt/raw/master/i2c-mqtt-server/src/main/resources/app.properties
     
```

```

#
# Sample Configuration
#
# I2C BUS Name (see PI4J I2CBus Constants)
i2c.busname=bus_1
#
#
# The i2c devices (separated by Comma)
# 
# <devicetype>:<instanceName>:<i2c-address>:<device-direction>:<inverted>
#
#   devicetype       = always "pcf8574"; No other implemented yet
#   instanceName     = unique Instance Name
#   i2c-address      = Address from device as HEX-String
#   device-direction = [input|output] The direction of the device (input or output); Only one per Device supported
#   inverted         = [true|false] Values read/write will be inverted 
#
i2c.devices=pcf8574:testInput:0x38:input:true,pcf8574:testOutput:0x68:output:true
#
#
# Leave Blank for default /i2c/
#
#
mqtt.topic.prefix=
#
# MQTT - Client - Settings (see mqtt paho API)
mqtt.server=192.168.20.151:1883
mqtt.retained=true
mqtt.qos=0
mqtt.automaticReconnect=true
mqtt.cleanSession=true
mqtt.connectionTimeout=10


```

## Connect i2c Devices 
* Connect the devices to the i2c BUS on Raspberry PI
* Connect the interrupt to GPIO_00

## Run the jar

```
java -jar i2c-mqtt-server-1.1.2.jar 
```

# Usage

## MQTT-Topics

### Input

Input changes are published to this topic

```
i2c/input/<instanceName>/[1-8]/STAT
```

Where "instanceName" is the definied Instance Name from app.properties and [1-8] the Port which is changed. The Payload is [ON] or [OFF]


**Example**
 
PIN 1 Input Value of Device with instanceName "myInputDevice" changed:

```
topic  : i2c/input/myInputDevice/1/STAT
payload: ON
```

### Output

The output Module subscribs to this topic:

```
i2c/output/<instanceName>/[1-8]/POWER

```

Where "instanceName" is the definied Instance Name from app.properties and [1-8] the Port which should be changed. 
The Payload must be "ON" to turn on and "OFF" to turn off.

Example: Turn on Pin 1 on Device with instanceName=myOutputDevice

```
topic: i2c/output/myOutputDevice/1/POWER
payload: ON

```